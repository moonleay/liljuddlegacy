package de.limited_dev.lil_judd;

import de.limited_dev.lil_judd.build.BuildConstants;
import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommandManager;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommandManager;
import de.limited_dev.lil_judd.features.maps.MapManager;
import de.limited_dev.lil_judd.features.planner.TimePlanner;
import de.limited_dev.lil_judd.features.storage.GuildTimePlannerStorage;
import de.limited_dev.lil_judd.features.weapons.WeaponManager;
import de.limited_dev.lil_judd.interactions.messagecontext.components.MessageContextInteractionManager;
import de.limited_dev.lil_judd.interactions.usercontext.components.UserContextInteractionManager;
import de.limited_dev.lil_judd.listeners.ContextInteractionListener;
import de.limited_dev.lil_judd.listeners.GuildStateListener;
import de.limited_dev.lil_judd.listeners.ReadyListener;
import de.limited_dev.lil_judd.listeners.SlashCommandInteractionListener;
import de.limited_dev.lil_judd.util.Logger;
import de.limited_dev.lil_judd.util.managers.TokenManager;
import de.limited_dev.lil_judd.util.managers.WeaponDataManager;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.managers.AudioManager;
import net.dv8tion.jda.api.requests.GatewayIntent;

public class Main {
    private static JDA jda;
    private static Logger lgr = new Logger();
    private static final TokenManager tokenManager = TokenManager.getInstance();
    private static final WeaponDataManager weaponDataManager = WeaponDataManager.getInstance();
    private static final GuildTimePlannerStorage gtps = GuildTimePlannerStorage.getInstance();
    private static long launchTime;

    public static void main(String[] args){
        lgr.info("li'l Judd is waking up ...");
        lgr.info(BuildConstants.botVersion);
        launchTime = System.currentTimeMillis();
        tokenManager.load();
        gtps.load();
        weaponDataManager.loadPatchInfo();

        jda = JDABuilder.createDefault(tokenManager.getToken())
                .setActivity(Activity.watching("v." + BuildConstants.botVersion + " / jda v." + BuildConstants.jdaVersion ))
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .build();

        jda.addEventListener(new ReadyListener());
        jda.addEventListener(new SlashCommandInteractionListener());
        jda.addEventListener(new ContextInteractionListener());
        jda.addEventListener(new GuildStateListener());

        try {
            jda.awaitReady();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                shutdown();
            }
        });

        MapManager.registerMaps();
        WeaponManager.registerWeapons();
        TimePlanner.registerMessageThread();
        JDACommandManager.registerCommands();
        UserContextInteractionManager.registerInteractions();
        MessageContextInteractionManager.registerInteractions();
        ConsoleCommandManager.registerCommands();
        ConsoleCommandManager.registerListener();
    }

    public static void shutdown(){
        lgr.consoleOut("Shutting down...");
        for(Guild g : jda.getSelfUser().getMutualGuilds()){
            Member self = g.getSelfMember();
            GuildVoiceState selfState = self.getVoiceState();
            if(selfState.inAudioChannel()){
                AudioManager audioManager = g.getAudioManager();
                lgr.consoleOut("Leaving VC " + audioManager.getConnectedChannel().asVoiceChannel().getName() + " in " + g.getName());
                audioManager.closeAudioConnection();
            }
        }
        TimePlanner.schedulerService.shutdown();
        jda.shutdown();
        lgr.consoleOut("Done.. Exiting..");
    }

    public static JDA getJda() {
        return jda;
    }

    public static Logger getLgr() {
        return lgr;
    }

    public static long getUptime(){
        return System.currentTimeMillis() - launchTime;
    }

    public static long getLaunchTime() {
        return launchTime;
    }

    public static WeaponDataManager getWeaponDataManager() {
        return weaponDataManager;
    }

    public static GuildTimePlannerStorage getGtps() {
        return gtps;
    }
}
