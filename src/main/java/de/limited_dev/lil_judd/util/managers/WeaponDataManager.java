package de.limited_dev.lil_judd.util.managers;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.build.BuildConstants;
import de.limited_dev.lil_judd.features.weapons.components.WeaponKit;
import de.limited_dev.lil_judd.features.weapons.components.enums.MainType;
import de.limited_dev.lil_judd.features.weapons.components.enums.SpecialType;
import de.limited_dev.lil_judd.features.weapons.components.enums.SubType;
import de.limited_dev.lil_judd.util.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class WeaponDataManager {
    private static final Logger lgr = Main.getLgr();
    private static WeaponDataManager weaponDataManager;
    private final String basePath = "." + File.separator + "data" + File.separator + "Weapons" + File.separator;

    private String basedOnSplatoonVersion;

    public static WeaponDataManager getInstance(){
        if(weaponDataManager == null){
            weaponDataManager = new WeaponDataManager();
        }
        return weaponDataManager;
    }

    public List<WeaponKit> load(){
        final String filename = "";
        final String filePath = basePath + filename;
        File dir = new File(basePath);
        if(!dir.exists()){
            return null;
        }
        List<WeaponKit> kits = new ArrayList<>();
        for(File f : dir.listFiles()){
            if(f.getName().contains(".info"))
                continue;
            if(!f.exists()){
                lgr.info("This file does not exists. This should not happen!");
                continue;
            }
            try{
                InputStream input = new FileInputStream(f.getAbsolutePath());
                Properties prop = new Properties();
                prop.load(input);

                kits.add(new WeaponKit(prop.getProperty("nameDE"),
                        prop.getProperty("nameEN"),
                        prop.getProperty("callout"),
                        Integer.parseInt(prop.getProperty("requiredLvl")),
                        Integer.parseInt(prop.getProperty("pointsForSpecial")),
                        MainType.getFromString(prop.getProperty("mainType")),
                        SubType.getFromString(prop.getProperty("subType")),
                        SpecialType.getFromString(prop.getProperty("specialType")),
                        prop.getProperty("imgLink")));
                lgr.info("Found: " + prop.getProperty("nameEN") + " in " + f.getName());
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(kits.size() == 0){
            lgr.info("No Weapons found!; Loading Null");
            return null;
        }
        lgr.info("Loaded " + kits.size() + " Weapons!");
        return kits;
    }

    public void loadPatchInfo(){
        final String filename = "version.info";
        final String filePath = basePath + filename;
        File dir = new File(basePath);
        if(!dir.exists()){
            savePatchInfo();
            return;
        }
        File configFile = new File(dir, filename);
        if(!configFile.exists()){
            savePatchInfo();
            return;
        }
        try{
            InputStream input = new FileInputStream(filePath);
            Properties prop = new Properties();

            prop.load(input);
            basedOnSplatoonVersion = prop.getProperty("basedOnSplatoonVersion");
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePatchInfo(){
        File dir = new File(basePath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        final String filename = "version.info";
        final String filePath = basePath + filename;
        File configFile = new File(dir, filename);
        if(!configFile.exists()){
            try{
                configFile.createNewFile();
            } catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            OutputStream output = new FileOutputStream(filePath);
            Properties prop = new Properties();

            prop.setProperty("basedOnSplatoonVersion", BuildConstants.splatoonPatchVersion);

            prop.store(output, null);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBasedOnSplatoonVersion() {
        return basedOnSplatoonVersion == null ? BuildConstants.splatoonPatchVersion : basedOnSplatoonVersion;
    }
}
