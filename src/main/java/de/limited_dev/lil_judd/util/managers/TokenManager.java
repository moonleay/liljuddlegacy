package de.limited_dev.lil_judd.util.managers;

import java.io.*;
import java.util.Properties;

public class TokenManager {

    private static TokenManager tokenManager;
    private final String basePath = "./data/";
    private final String filename = "token.judd";
    private final String filePath = basePath + filename;
    private String token;

    public static TokenManager getInstance(){
        if(tokenManager == null){
            tokenManager = new TokenManager();
        }
        return tokenManager;
    }

    public void load(){
        File dir = new File(basePath);
        if(!dir.exists()){
            save();
            return;
        }
        File configFile = new File(dir, filename);
        if(!configFile.exists()){
            save();
            return;
        }
        try{
            InputStream input = new FileInputStream(filePath);
            Properties prop = new Properties();

            prop.load(input);
            token = prop.getProperty("token").equals("empty") ? null : prop.getProperty("token");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(){
        File dir = new File(basePath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File configFile = new File(dir, filename);
        if(!configFile.exists()){
            try{
                configFile.createNewFile();
            } catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            OutputStream output = new FileOutputStream(filePath);
            Properties prop = new Properties();

            prop.setProperty("token", "empty");

            prop.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getToken() {
        return token;
    }
}
