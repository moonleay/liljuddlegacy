package de.limited_dev.lil_judd.util;

import de.limited_dev.lil_judd.Main;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class SlashCommandRegister {

    public static void registerCommands(){

        Main.getJda().updateCommands().addCommands(
                Commands.slash("info", "Shows Info about the bot")
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("setup", "Setup a feature")
                        .addOptions(
                                new OptionData(OptionType.STRING, "feature", "The Feature you want to setup", true)
                                        .addChoice("Send Time finder", "timefinder"),
                                new OptionData(OptionType.CHANNEL, "channel", "The Channel the feature will post in.", true)),
                Commands.slash("remove", "Remove a feature")
                        .addOptions(new OptionData(OptionType.STRING, "feature", "The Feature you want to remove", true)
                                        .addChoice("Send Time finder", "timefinder"),
                                    new OptionData(OptionType.CHANNEL, "channel", "The Channel with the feature", true)),
                /*
                Commands.slash("searchscrim", "Search for a scrim")
                        .addOptions(new OptionData(OptionType.INTEGER, "div", "The Division you want to search in", true),
                                    new OptionData(OptionType.USER, "player1", "The first Player of the team", true),
                                    new OptionData(OptionType.USER, "player2", "The second Player of the team", true),
                                    new OptionData(OptionType.USER, "player3", "The third Player of the team", true),
                                    new OptionData(OptionType.USER, "player4", "The fourth Player of the team", true)),
                Commands.slash("endscrim", "Quit a scrum / Leave the queue"),
                Commands.slash("msgscrim", "Msg the enemy team")
                        .addOptions(new OptionData(OptionType.STRING, "message", "Message content", true)),
                Commands.slash("scriminfo", "Show info about the scrim system."),
                 */
                Commands.slash("mapinfo", "Show map information")
                        .addOptions(new OptionData(OptionType.STRING, "map", "Map Name", true)),
                Commands.slash("weaponinfo", "Get infos about a weapon")
                        .addOptions(new OptionData(OptionType.STRING, "name", "Weapon name in English or German", true))
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("randomweapon", "Get a random weapon")
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices")),
                Commands.slash("help", "Shows all commands & their descriptions")
                        .addOptions(new OptionData(OptionType.BOOLEAN, "mobileformatting", "Format the reply for mobile devices"))
                //Commands.context(Command.Type.USER, "Get user avatar"),
                //Commands.message("Get message ID")
        ).queue();
    }
    //https://jda.wiki/using-jda/interactions/#context-menus

}
