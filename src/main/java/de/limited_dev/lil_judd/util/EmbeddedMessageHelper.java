package de.limited_dev.lil_judd.util;

import de.limited_dev.lil_judd.Main;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.annotation.Nullable;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class EmbeddedMessageHelper {
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy @ HH:mm:ss");

    public static void sendWithTable(MessageReceivedEvent event, String author, String title, Color color, String imageUrl,
                                     String description, @Nullable String[] titleOfFlields, @Nullable HashMap<String, String[]> valueOfFlieds, boolean inline, boolean fromMobile, int deleteDelay) {
        LocalDateTime now = LocalDateTime.now();
        EmbedBuilder eb = new EmbedBuilder();
        eb.setAuthor(author);
        eb.setTitle(title);
        eb.setColor(color);
        eb.setDescription(description);
        if (titleOfFlields != null) {
            if (!fromMobile) {
                for (String titleOfFlied : titleOfFlields) {
                    String value = "";
                    for (String s : valueOfFlieds.get(titleOfFlied)) {
                        value = value.isEmpty() ? s : value + "\n" + s;
                    }
                    eb.addField(titleOfFlied, value, inline);
                }
            } else { // is from mobile
                String tof = "";
                String vof = "";
                for (int i = 0; i < titleOfFlields.length; ++i) {
                    tof += titleOfFlields[i] + (i == titleOfFlields.length - 1 ? "" : " -- ");
                }
                if (!valueOfFlieds.isEmpty()) {
                    for (int i = 0; i < valueOfFlieds.get(titleOfFlields[0]).length; ++i) {
                        for (int j = 0; j < titleOfFlields.length; ++j) {
                            vof += valueOfFlieds.get(titleOfFlields[j])[i] + (j == titleOfFlields.length - 1 ? "" : " == ");
                        }
                        vof += "\n"; //TODO finish
                    }
                    eb.addField(tof, vof, inline);
                }
            }
        }

        if (imageUrl != null && !fromMobile) {
            eb.setThumbnail(imageUrl);
        }

        eb.setFooter(">" + dtf.format(now) + " - " + event.getMessage().getAuthor().getName() + "#" + event.getMessage().getAuthor().getDiscriminator());
        if (deleteDelay == 0) {
            event.getChannel().sendMessageEmbeds(eb.build()).queue();
            return;
        }
        event.getChannel().sendMessageEmbeds(eb.build()).delay(deleteDelay, TimeUnit.SECONDS).flatMap(Message::delete).queue();
    }

    public static void sendWithTable(SlashCommandInteractionEvent event, String author, String title, Color color, String imageUrl,
                                     String description, @Nullable String[] titleOfFlields, @Nullable HashMap<String, String[]> valueOfFlieds, boolean inline) {
        boolean fromMobile;
        if(event.getOption("mobileformatting") != null){
            fromMobile = event.getOption("mobileformatting").getAsBoolean();
        }else{
            fromMobile = false;
        }


        LocalDateTime now = LocalDateTime.now();
        EmbedBuilder eb = new EmbedBuilder();
        eb.setAuthor(author);
        eb.setTitle(title);
        eb.setColor(color);
        eb.setDescription(description);
        if (titleOfFlields != null) {
            if (!fromMobile) {
                for (String titleOfFlied : titleOfFlields) {
                    String value = "";
                    for (String s : valueOfFlieds.get(titleOfFlied)) {
                        value = value.isEmpty() ? s : value + "\n" + s;
                    }
                    eb.addField(titleOfFlied, value, inline);
                }
            } else { // is from mobile
                String tof = "";
                String vof = "";
                for (int i = 0; i < titleOfFlields.length; ++i) {
                    tof += titleOfFlields[i] + (i == titleOfFlields.length - 1 ? "" : " -- ");
                }
                if (!valueOfFlieds.isEmpty()) {
                    for (int i = 0; i < valueOfFlieds.get(titleOfFlields[0]).length; ++i) {
                        for (int j = 0; j < titleOfFlields.length; ++j) {
                            vof += valueOfFlieds.get(titleOfFlields[j])[i] + (j == titleOfFlields.length - 1 ? "" : " == ");
                        }
                        vof += "\n"; //TODO finish
                    }
                    eb.addField(tof, vof, inline);
                }
            }
        }

        if (imageUrl != null && !fromMobile) {
            eb.setThumbnail(imageUrl);
        }

        eb.setFooter(">" + dtf.format(now) + " - " + event.getUser().getName() + "#" + event.getUser().getDiscriminator());
        event.replyEmbeds(eb.build()).queue();
    }

    public static void sendSimpleOneLiner(SlashCommandInteractionEvent event, String title, String description) {
        sendSimpleOneLiner(event, title, description, null);
    }

    public static void sendSimpleOneLiner(SlashCommandInteractionEvent event, String title, String description, String thumbnailURL) {
        LocalDateTime now = LocalDateTime.now();
        EmbedBuilder eb = new EmbedBuilder();

        eb.setAuthor(Main.getJda().getSelfUser().getName());
        eb.setTitle(title);
        eb.setColor(Color.ORANGE);
        eb.setDescription(description);
        if (thumbnailURL != null)
            eb.setThumbnail(thumbnailURL);
        eb.setFooter(">" + dtf.format(now) + " - " + event.getUser().getName() + "#" + event.getUser().getDiscriminator());
        event.replyEmbeds(eb.build()).queue();
    }

    public static void sendSimpleOneLiner(Guild g, long channelID, String title, String description, String thumbnailURL){
        LocalDateTime now = LocalDateTime.now();
        EmbedBuilder eb = new EmbedBuilder();

        eb.setAuthor(Main.getJda().getSelfUser().getName());
        eb.setTitle(title);
        eb.setColor(Color.ORANGE);
        eb.setDescription(description);
        if(thumbnailURL != null)
            eb.setThumbnail(thumbnailURL);
        eb.setFooter(">" + dtf.format(now) + " - Automated Message");
        g.getTextChannelById(channelID).sendMessageEmbeds(eb.build()).queue();
    }

    public static void sendSimpleOneLiner(Guild g, long channelID, String title, String description, String thumbnailURL, int delay){
        LocalDateTime now = LocalDateTime.now();
        EmbedBuilder eb = new EmbedBuilder();

        eb.setAuthor(Main.getJda().getSelfUser().getName());
        eb.setTitle(title);
        eb.setColor(Color.ORANGE);
        eb.setDescription(description);
        if(thumbnailURL != null)
            eb.setThumbnail(thumbnailURL);
        eb.setFooter(">" + dtf.format(now) + " - Automated Message");
        g.getTextChannelById(channelID).sendMessageEmbeds(eb.build()).queueAfter(delay, TimeUnit.SECONDS);
    }

    public static EmbedBuilder getEmbeddedMessageAutomated(boolean doAuthor, String title, String description, String thumbnailURL, boolean shouldAddFooter){
        LocalDateTime now = LocalDateTime.now();
        EmbedBuilder eb = new EmbedBuilder();
        if(doAuthor)
            eb.setAuthor(Main.getJda().getSelfUser().getName());
        if(title != null){
            eb.setTitle(title);
        }
        eb.setColor(Color.ORANGE);
        if(description != null)
            eb.setDescription(description);
        if(thumbnailURL != null)
            eb.setThumbnail(thumbnailURL);
        if(shouldAddFooter)
            eb.setFooter(">" + dtf.format(now) + " - Automated Message");
        return eb;
    }
}
