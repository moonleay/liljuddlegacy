package de.limited_dev.lil_judd.util;

import java.util.HashMap;
import java.util.Map;

public class DayUtil {
    private static final Map<String, Integer> DaysUntilMonday = new HashMap<>(){{
        put("MONDAY", 0);
        put("TUESDAY", 6);
        put("WEDNESDAY", 5);
        put("THURSDAY", 4);
        put("FRIDAY", 3);
        put("SATURDAY", 2);
        put("SUNDAY", 1);
    }};

    public static int getDelay(String day){
        return DaysUntilMonday.get(day);
    }
}
