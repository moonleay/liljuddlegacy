package de.limited_dev.lil_judd.features.maps.components;

public class MapData {
    private final String nameDE;
    private final String nameEN;
    private final String mapLink;

    public MapData(String nameEN, String nameDE, String mapLink) {
        this.nameEN = nameEN;
        this.nameDE = nameDE;
        this.mapLink = mapLink;
    }

    public boolean equalsDEName(String str) {
        return this.nameDE.toLowerCase().equals(str);
    }

    public boolean startsWithDEName(String str){
        return this.nameDE.toLowerCase().startsWith(str);
    }

    public boolean startsWithENName(String str){
        return this.nameEN.toLowerCase().startsWith(str);
    }

    public boolean equalsENName(String str) {
        return this.nameEN.toLowerCase().equals(str);
    }

    public String getNameDE() {
        return this.nameDE;
    }

    public String getNameEN() {
        return this.nameEN;
    }

    public String getMapLink() {
        return mapLink;
    }
}
