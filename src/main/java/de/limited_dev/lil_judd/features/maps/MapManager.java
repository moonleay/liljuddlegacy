package de.limited_dev.lil_judd.features.maps;

import de.limited_dev.lil_judd.features.maps.components.MapData;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MapManager {
    private static List<MapData> maps = new CopyOnWriteArrayList<>();

    public static void registerMaps() {
        maps.add(new MapData("Scorch Gorge", "Sengkluft", "https://cdn.wikimg.net/en/splatoonwiki/images/thumb/c/c2/S3_promo_screenshot_Scorch_Gorge_00.jpg/400px-S3_promo_screenshot_Scorch_Gorge_00.jpg"));
        maps.add(new MapData("Eeltail Alley", "Streifenaal-Straße", "https://cdn.wikimg.net/en/splatoonwiki/images/thumb/8/84/S3_promo_screenshot_Eeltail_Alley_00.jpg/400px-S3_promo_screenshot_Eeltail_Alley_00.jpg"));
        maps.add(new MapData("Hagglefish Market", "Schnapperchen-Basar", "https://cdn.wikimg.net/en/splatoonwiki/images/2/2e/S3HagglefishMarketIcon.webp"));
        maps.add(new MapData("Undertow Spillway", "Schwertmuschel-Reservoir", "https://cdn.wikimg.net/en/splatoonwiki/images/f/f2/S3UndertowSpillwayIcon.webp"));
        maps.add(new MapData("Mincemeat Metalworks", "Aalstahl-Metallwerk", "https://cdn.wikimg.net/en/splatoonwiki/images/a/a8/S3MincemeatMetalworksIcon.webp"));
        maps.add(new MapData("Hammerhead Bridge", "Makrelenbrücke", "https://cdn.wikimg.net/en/splatoonwiki/images/2/24/S3HammerheadBridgeIcon.jpeg"));
        maps.add(new MapData("Museum d'Alfonsino", "Pinakoithek", "https://cdn.wikimg.net/en/splatoonwiki/images/2/2c/S3_Stage_Museum_d%27Alfonsino_Promo_1.jpg"));
        maps.add(new MapData("Mahi-Mahi Resort", "Mahi-Mahi Resort", "https://cdn.wikimg.net/en/splatoonwiki/images/b/b7/S3MahiMahiResortIcon.jpeg"));
        maps.add(new MapData("Inkblot Art Academy", "Perlmutt-Akademie", "https://cdn.wikimg.net/en/splatoonwiki/images/9/9e/S3_Inkblot_Art_Academy.jpeg"));
        maps.add(new MapData("Sturgeon Shipyard", "Störwerft", "https://cdn.wikimg.net/en/splatoonwiki/images/a/a5/S3_Sturgeon_Shipyard.jpg"));
        maps.add(new MapData("MakoMart", "Cetacea-Markt", "https://cdn.wikimg.net/en/splatoonwiki/images/a/ac/S3_Mako_Mart.jpg"));
        maps.add(new MapData("Wahoo World", "Flunder-Funpark", "https://cdn.wikimg.net/en/splatoonwiki/images/5/53/S3_Wahoo_World.jpg"));
        maps.add(new MapData("Brinewater Springs", "Kusaya-Quellen", "https://cdn.wikimg.net/en/splatoonwiki/images/b/bf/S3_Brinewater_Springs.png"));
        maps.add(new MapData("Flounder Heights", "Schollensiedlung", "https://cdn.wikimg.net/en/splatoonwiki/images/2/23/S3_Stage_Flounder_Heights.png"));
    }

    public static MapData getMapData(String name) {
        for (MapData md : maps) {
            if (md.startsWithDEName(name))
                return md;
            if (md.startsWithENName(name))
                return md;
            if(md.equalsDEName(name))
                return md;
            if(md.equalsENName(name))
                return md;
        }
        return null;
    }
}
