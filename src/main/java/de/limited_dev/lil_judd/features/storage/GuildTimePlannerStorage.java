package de.limited_dev.lil_judd.features.storage;

import de.limited_dev.lil_judd.Main;
import net.dv8tion.jda.api.entities.Guild;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class GuildTimePlannerStorage {
    private static GuildTimePlannerStorage guildTimePlannerStorage;
    private final String basePath = "./data/GuildTimePlanner/";
    private final String filename = "guildsLinkedToChannels.judd";
    private final String filePath = basePath + filename;
    public Map<Long, Long> guildsWithPlanner = new HashMap<>();

    public static GuildTimePlannerStorage getInstance(){
        if(guildTimePlannerStorage == null){
            guildTimePlannerStorage = new GuildTimePlannerStorage();
        }
        return guildTimePlannerStorage;
    }

    public void load(){
        File dir = new File(basePath);
        if(!dir.exists()){
            save();
            return;
        }
        File file = new File(dir, filename);
        if(!file.exists()){
            save();
            return;
        }
        try{
            InputStream input = new FileInputStream(filePath);
            Properties prop = new Properties();

            prop.load(input);

            for(Object o : prop.keySet()){
                guildsWithPlanner.put(Long.parseLong(o.toString()), Long.parseLong(prop.get(o).toString()));
                Main.getLgr().info("key: " + o.toString() + ", val: " + prop.getProperty(o.toString()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(){
        File dir = new File(basePath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File file = new File(dir, filename);
        if(!file.exists()){
            try{
                file.createNewFile();
            } catch(IOException e){
                e.printStackTrace();
            }
        }

        try{
            OutputStream output = new FileOutputStream(filePath);
            Properties prop = new Properties();

            //prop.setProperty("token", "empty");
            for(long l : guildsWithPlanner.keySet()){
                prop.setProperty(String.valueOf(l), guildsWithPlanner.get(l).toString());
            }
            prop.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
