package de.limited_dev.lil_judd.features.weapons.components.enums;

import de.limited_dev.lil_judd.Main;

import java.util.Objects;

public enum SubType {
    SUCTION_BOMB("Suction Bomb"),
    SPLAT_BOMB("Splat Bomb"),
    CURLING_BOMB("Curling Bomb"),
    AUTO_BOMB("Autobomb"),
    SPRINKLER("Sprinkler"),
    TOXIC_MIST("Toxic Mist"),
    FIZZY_BOMB("Fizzy Bomb"),
    TORPEDO("Torpedo"),
    INK_MINE("Ink Mine"),
    POINT_SENSOR("Point Sensor"),
    ANGLE_SHOOTER("Angle Shooter"),
    SPLASH_WALL("Splash Wall"),
    BURST_BOMB("Burst Bomb"),
    SQUID_BEAKON("Squid Beakon");

    private final String nameEN;

    SubType(String nameEN){
        this.nameEN = nameEN;
    }

    public static SubType getFromString(String str){
        for(SubType s : SubType.values()){
            if(Objects.equals(s.nameEN.toLowerCase(), str.toLowerCase()))
                return s;
        }
        Main.getLgr().info("Could not find subtype");
        return null;
    }

    public String getNameEN() {
        return nameEN;
    }
}
