package de.limited_dev.lil_judd.features.weapons.components;

import de.limited_dev.lil_judd.features.weapons.components.enums.MainType;
import de.limited_dev.lil_judd.features.weapons.components.enums.SubType;
import de.limited_dev.lil_judd.features.weapons.components.enums.SpecialType;

public class WeaponKit {
    private final String nameDE;
    private final String nameEN;
    private final String callout;
    private final int reqiredLevel;
    private final int pointsForSpecial;
    private final MainType mainType;
    private final SubType subType;
    private final SpecialType specialType;
    private final String imageLink;

    public WeaponKit(String nameDE, String nameEN, String callout, int requiredLvl, int pointsForSpecial, MainType mainType, SubType subType, SpecialType specialType, String imageLink){
        this.nameDE = nameDE;
        this.nameEN = nameEN;
        this.callout = callout;
        this.reqiredLevel = requiredLvl;
        this.pointsForSpecial = pointsForSpecial;
        this.mainType = mainType;
        this.subType = subType;
        this.specialType = specialType;
        this.imageLink = imageLink;
    }

    public String getNameDE() {
        return nameDE;
    }

    public String getCallout() {
        return callout;
    }

    public int getReqiredLevel() {
        return reqiredLevel;
    }

    public int getPointsForSpecial() {
        return pointsForSpecial;
    }

    public MainType getMainType() {
        return mainType;
    }

    public SubType getSubType() {
        return subType;
    }

    public SpecialType getSpecialType() {
        return specialType;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getNameEN() {
        return nameEN;
    }
}
