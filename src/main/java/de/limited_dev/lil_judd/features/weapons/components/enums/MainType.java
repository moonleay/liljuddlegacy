package de.limited_dev.lil_judd.features.weapons.components.enums;

import de.limited_dev.lil_judd.Main;

import java.util.Objects;

public enum MainType {
    SHOOTER("Shooter"),
    CHARGER("Charger"),
    ROLLER("Roller"),
    BLASTER("Blaster"),
    SLOSHER("Slosher"),
    DUALIE("Dualie"),
    SPLATLING("Splatling"),
    BRUSH("Brush"),
    STRINGER("Stringer"),
    BRELLA("Brella"),
    SPLATANA("Splatana");

    private String type;

    MainType(String type){
        this.type = type;
    }

    public static MainType getFromString(String str){
        for(MainType m : MainType.values()){
            if(Objects.equals(m.getType().toLowerCase(), str.toLowerCase()))
                return m;
        }
        Main.getLgr().info("Could not find maintype");
        return null;
    }

    public String getType() {
        return type;
    }
}
