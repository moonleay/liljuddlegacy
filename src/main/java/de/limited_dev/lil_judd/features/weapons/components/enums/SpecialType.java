package de.limited_dev.lil_judd.features.weapons.components.enums;

import de.limited_dev.lil_judd.Main;

import java.util.Objects;

public enum SpecialType {
    TRIZOOKA("Trizooka"),
    BIG_BUBBLER("Big Bubbler"),
    INK_VAC("Ink Vac"),
    TRIPLE_INKSTRIKE("Triple Inkstrike"),
    CRAB_TANK("Crab Tank"),
    WAVE_BREAKER("Wave Breaker"),
    ZIPCASTER("Zipcaster"),
    KILLER_WAIL_5_1("Killer Wail 5.1"),
    REEFSLIDER("Reefslider"),
    ULTRA_STAMP("Ultra Stamp"),
    TACTICOOLER("Tacticooler"),
    INKJET("Inkjet"),
    BOOYAH_BOMB("Booyah Bomb"),
    INK_STORM("Ink Storm"),
    TENTA_MISSILS("Tenta Missiles");

    private final String nameEN;

    SpecialType(String nameEN){
        this.nameEN = nameEN;
    }

    public static SpecialType getFromString(String str){
        for(SpecialType s : SpecialType.values()){
            if(Objects.equals(s.getNameEN().toLowerCase(), str.toLowerCase()))
                return s;
        }
        Main.getLgr().info("Could not find specialtype");
        return null;
    }

    public String getNameEN() {
        return nameEN;
    }
}
