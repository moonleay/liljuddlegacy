package de.limited_dev.lil_judd.features.weapons;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.features.weapons.components.WeaponKit;
import de.limited_dev.lil_judd.features.weapons.components.enums.MainType;
import de.limited_dev.lil_judd.features.weapons.components.enums.SpecialType;
import de.limited_dev.lil_judd.features.weapons.components.enums.SubType;
import de.limited_dev.lil_judd.util.Logger;
import de.limited_dev.lil_judd.util.managers.WeaponDataManager;

import java.util.ArrayList;
import java.util.List;

public class WeaponManager {
    private static List<WeaponKit> weaponKitList = new ArrayList<>();
    private static Logger lgr = Main.getLgr();
    public static void registerWeapons(){
        //weaponKitList.add(new WeaponKit(".52 Gallon", ".52 Gal", 11, 200, MainType.SHOOTER, SubType.SPLASH_WALL, SpecialType.KILLER_WAIL_5_1, "https://cdn.wikimg.net/en/splatoonwiki/images/e/ed/S3_Weapon_Main_.52_Gal.png"));
        weaponKitList = Main.getWeaponDataManager().load();
    }

    public static void reloadWeapons(){
        weaponKitList = Main.getWeaponDataManager().load();
        lgr.info("Reloaded all Weapons.");
    }

    public static WeaponKit getExactWeaponKit(String name){
        for(WeaponKit wk : weaponKitList){
            if(wk.getCallout().toLowerCase().equals(name.toLowerCase())){
                return wk;
            }
            if(wk.getNameDE().toLowerCase().equals(name.toLowerCase())){
                return wk;
            }
            if(wk.getNameEN().toLowerCase().equals(name.toLowerCase())){
                return wk;
            }
        }
        return null;
    }

    public static WeaponKit getWeaponKitFromSearch(String name){
        for(WeaponKit wk : weaponKitList){
            if(wk.getCallout().toLowerCase().startsWith(name.toLowerCase())){
                return wk;
            }
            if(wk.getNameDE().toLowerCase().contains(name.toLowerCase())){
                return wk;
            }
            if(wk.getNameEN().toLowerCase().contains(name.toLowerCase())){
                return wk;
            }
            if(wk.getCallout().toLowerCase().contains(name.toLowerCase())){
                return wk;
            }
        }
        return null;
    }

    public static WeaponKit getIndex(int index){
        return weaponKitList.get(index);
    }

    public static int getSize(){
        return weaponKitList.size();
    }
}
