package de.limited_dev.lil_judd.features.planner;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.features.storage.GuildTimePlannerStorage;
import de.limited_dev.lil_judd.util.DayUtil;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import de.limited_dev.lil_judd.util.Logger;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.emoji.Emoji;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimePlanner {
    private static final GuildTimePlannerStorage gtps = Main.getGtps();
    private static final Logger lgr = Main.getLgr();
    public static final int hour = 8; // 8
    public static final int minute = 0; // 0
    public static final int second = 0; // 0
    public static ScheduledExecutorService schedulerService;

    public static GuildTimePlannerStorage getGtps() {
        return gtps;
    }

    public static void registerMessageThread() {
        // get the current ZonedDateTime of your TimeZone
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

        ZonedDateTime nextNotifyDay = now.plusDays(DayUtil.getDelay(now.getDayOfWeek().name())).withHour(hour).withMinute(minute).withSecond(second);
        //ZonedDateTime nextNotifyDay = now.plusSeconds(10);
        lgr.info(nextNotifyDay.getDayOfWeek() + ", " + nextNotifyDay.getDayOfMonth() + "." + nextNotifyDay.getMonth() + "." + nextNotifyDay.getYear() + " / " + nextNotifyDay.getHour() + ":" + nextNotifyDay.getMinute() + ":" + nextNotifyDay.getSecond());

        //lgr.info(now.getDayOfWeek().name() + " " + nextNotifyDay.getDayOfWeek().name());

        // if it's already past the time (in this case 8:05) the first lesson will be scheduled for the next day
        if (now.compareTo(nextNotifyDay) > 0) {
            nextNotifyDay = nextNotifyDay.plusDays(7).withHour(hour).withMinute(minute).withSecond(second);
            lgr.info("Its already after 8am. Setting time to next Week");
        }

        // duration between now and the beginning of the next first lesson
        Duration timeUntilNextNotification = Duration.between(now, nextNotifyDay);
        // in seconds
        long initialDelayUntilNextNotification = timeUntilNextNotification.getSeconds();

        lgr.info("Duration: " + initialDelayUntilNextNotification);

        // schedules the reminder at a fixed rate of one day
        schedulerService = Executors.newScheduledThreadPool(1);
        schedulerService.scheduleAtFixedRate(() -> {
                    // send a message
                    JDA jda = Main.getJda();
                    Set<Long> gs = gtps.guildsWithPlanner.keySet();
                    lgr.info("All Guilds found with time planner:");
                    for (Long l : gs) {
                        if (jda.getGuildById(l) == null) {
                            lgr.info("Guild with id " + l + " does not exist.");
                            continue;
                        }
                        lgr.info("Guild: " + jda.getGuildById(l).getName());
                    }
                    Thread thr = new Thread() {
                        @Override
                        public void run() {
                            for (Long l : gs) {
                                if (jda.getGuildById(l) == null) {
                                    continue;
                                }
                                if (jda.getGuildById(l).getTextChannelById(gtps.guildsWithPlanner.get(l)) == null) {
                                    lgr.info("The channel with the id " + gtps.guildsWithPlanner.get(l) + " does not exist.");
                                    continue;
                                }
                                ZonedDateTime then = ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(hour).withMinute(minute).withSecond(second);
                                EmbeddedMessageHelper.sendSimpleOneLiner(jda.getGuildById(l), gtps.guildsWithPlanner.get(l), "Timeplanning System", "Do you have time on the following Days?", null);
                                try {
                                    Thread.sleep(4000);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                                for (int i = 0; i < 7; ++i) {
                                    if (i != 0)
                                        then = then.plusDays(1).withHour(hour).withMinute(minute).withSecond(second);
                                    MessageEmbed eb = EmbeddedMessageHelper.getEmbeddedMessageAutomated(false, null, then.getDayOfWeek() + ", " + then.getDayOfMonth() + "." + then.getMonthValue() + "." + then.getYear(), null, false).build();
                                    jda.getGuildById(l).getTextChannelById(gtps.guildsWithPlanner.get(l)).sendMessageEmbeds(eb).queue(this::addReactions);
                                    lgr.info(gs + ": " + then.getDayOfWeek() + ", " + then.getDayOfMonth() + "." + then.getMonthValue() + "." + then.getYear());
                                    try {
                                        Thread.sleep(5000);
                                    } catch (InterruptedException e) {
                                        throw new RuntimeException(e);
                                    }
                                }

                                lgr.info("Sent to Server " + jda.getGuildById(l).getName());
                            }
                            lgr.info("Terminating Thread...");
                            Thread.currentThread().interrupt();
                            return;
                        }

                        private void addReactions(Message msg){
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                            msg.addReaction(Emoji.fromUnicode("✅")).queue();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                            msg.addReaction(Emoji.fromFormatted("❌")).queue();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                            msg.addReaction(Emoji.fromFormatted("❓")).queue();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    };
                    thr.start();
                    lgr.info("Started sending Push Notifications for time Management");
                },
                initialDelayUntilNextNotification,
                TimeUnit.DAYS.toSeconds(7),
                TimeUnit.SECONDS);

    }
}
