package de.limited_dev.lil_judd.features.scrims;

import de.limited_dev.lil_judd.features.scrims.components.GuildInQueue;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ScrimMaker {
    private static List<GuildInQueue> queueList = new CopyOnWriteArrayList<>();
    private static List<GuildInQueue> gameList = new CopyOnWriteArrayList<>();

    public static void EnterQueue(Guild g, TextChannel channel, int div, List<User> users){
        if(IsInGameOrQueue(g)){
            EmbeddedMessageHelper.sendSimpleOneLiner(g, channel.getIdLong(), "Error", "You are already in a scrim or are already searching. Please stop searching first.", null);
            return;
        }
        boolean b = false;
        for(GuildInQueue gq : queueList){
            if(gq.getDiv() == div || b){
                b = true;

                EnterGame(gq, new GuildInQueue(g, channel, div, users));
            }
        }
        if(b)
            return;
        queueList.add(new GuildInQueue(g, channel, div, users));
    }

    public static void LeaveQueue(Guild g){
        for(GuildInQueue gq : queueList){
            if(gq.getGuild().equals(g)){
                EndGame(gq, null);
                return;
            }
        }
        for(GuildInQueue gq : gameList){
            if(gq.getGuild().equals(g)){
                EndGame(gq, gq.getEnemy());
            }
        }
    }

    public static void sendMessageToEnemy(Guild g, User u, String msg){
        for(GuildInQueue gq : gameList){
            if(gq.getGuild().equals(g)){
                gq.getEnemy().sendMessageInResponseChannel(msg, u);
            }
        }
    }

    public static boolean IsInQueue(Guild g){
        for(GuildInQueue gq : queueList)
            if(gq.getGuild().equals(g))
                return true;
        return false;
    }

    public static boolean IsInGame(Guild g){
        for(GuildInQueue gq : gameList)
            if(gq.getGuild().equals(g))
                return true;
        return false;
    }

    public static boolean IsInGameOrQueue(Guild g){
        return IsInQueue(g) || IsInGame(g);
    }

    public static boolean IsUserInGame(Guild g, User u){
        for(GuildInQueue gq : gameList){
            if(gq.getGuild().equals(g)){
                return gq.hasUserInIt(u);
            }
        }
        return false;
    }

    public static boolean IsUserInGameOrQueue(Guild g, User u){
        for(GuildInQueue gq : queueList){
            if(gq.getGuild().equals(g)){
                return gq.hasUserInIt(u);
            }
        }
        for(GuildInQueue gq : gameList){
            if(gq.getGuild().equals(g)){
                return gq.hasUserInIt(u);
            }
        }
        return false;
    }

    public static Guild getEnemyGuild(Guild g){
        for(GuildInQueue gq : gameList){
            if(gq.getGuild().equals(g))
                return gq.getEnemy().getGuild();
        }
        return null;
    }

    private static void EnterGame(GuildInQueue gq1, GuildInQueue gq2){
        queueList.remove(gq1);
        gameList.add(gq1);
        gameList.add(gq2);
        gq1.startScrimWith(gq2);
        gq2.startScrimWith(gq1);
    }

    private static void EndGame(GuildInQueue self, GuildInQueue withGuildinQueue){
        if(withGuildinQueue != null){
            withGuildinQueue.endScrim();
            gameList.remove(self);
            gameList.remove(withGuildinQueue);
            return;
        }
        queueList.remove(self);
        self.sendMessageInResponseChannel("You have been removed from the Queue.", null);
    }

    public static int getQueueSize(){
        return queueList.size();
    }

    public static int getGameSize(){
        return gameList.size();
    }
}
