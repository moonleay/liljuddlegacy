package de.limited_dev.lil_judd.features.scrims.components;

import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import de.limited_dev.lil_judd.util.TimeUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;

import java.util.List;

public class GuildInQueue {
    private final Guild g;
    private GuildInQueue enemyGuildInQueue;
    private final TextChannel responseChannel;
    private final int div;
    private final List<User> users;
    private final long startedSearching;
    private long startedScrim;

    public GuildInQueue(Guild g, TextChannel responseChannel, int div, List<User> users){
        this.g = g;
        this.responseChannel = responseChannel;
        this.div = div;
        this.users = users;
        this.startedSearching = System.currentTimeMillis();
    }

    public void startScrimWith(GuildInQueue g2){
        this.enemyGuildInQueue = g2;
        this.startedScrim = System.currentTimeMillis();
        EmbeddedMessageHelper.sendSimpleOneLiner(this.g, this.responseChannel.getIdLong(),
                "Match found.", "***A Scrim has been found!***\n" +
                        "Time spent queuing: " + TimeUtil.getTimeFormatedRaw(this.startedScrim - this.startedSearching) + "\n\n" +
                        "**Vs: " + this.enemyGuildInQueue.getGuild().getName() + "**\n" +
                        "Player 1 >> " + this.enemyGuildInQueue.getUsers().get(0).getAsMention() + "\n" +
                        "Player 2 >> " + this.enemyGuildInQueue.getUsers().get(1).getAsMention() + "\n" +
                        "Player 3 >> " + this.enemyGuildInQueue.getUsers().get(2).getAsMention() + "\n" +
                        "Player 4 >> " + this.enemyGuildInQueue.getUsers().get(3).getAsMention() +
                        "\n\n" +
                "/msgscrim <msg> to message the other Team\n" +
                        "/endscrim to end the current scrim", this.enemyGuildInQueue.getGuild().getIconUrl(), 2);
    }

    public void endScrim(){
        EmbeddedMessageHelper.sendSimpleOneLiner(this.g, this.responseChannel.getIdLong(), "The Scrim has ended.", "The Scrim with " + this.enemyGuildInQueue.getGuild().getName() + " ended.", this.enemyGuildInQueue.getGuild().getIconUrl());
    }


    public void sendMessageInResponseChannel(String msg, User u){
        if(u == null){
            EmbeddedMessageHelper.sendSimpleOneLiner(this.g, this.responseChannel.getIdLong(), "Info", "System >> " + msg, null);
            return;
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(this.g, this.responseChannel.getIdLong(), "Message from " + this.enemyGuildInQueue.getGuild().getName(), u.getName() + "#" + u.getDiscriminator() + " >> " + msg, u.getAvatarUrl());
    }

    public boolean hasUserInIt(User u){
        return users.contains(u);
    }

    public List<User> getUsers() {
        return users;
    }

    public GuildInQueue getEnemy(){
        return this.enemyGuildInQueue;
    }

    public int getDiv(){
        return this.div;
    }

    public Guild getGuild() {
        return g;
    }

    public TextChannel getResponseChannel(){
        return this.responseChannel;
    }
}
