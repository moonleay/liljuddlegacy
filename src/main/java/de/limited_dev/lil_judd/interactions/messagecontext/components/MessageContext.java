package de.limited_dev.lil_judd.interactions.messagecontext.components;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.util.Logger;
import net.dv8tion.jda.api.events.interaction.command.MessageContextInteractionEvent;
import org.jetbrains.annotations.NotNull;

public class MessageContext {

    private final String name;
    protected final Logger lgr = Main.getLgr();

    public MessageContext(String name) {
        this.name = name;
        lgr.info("Created UserContextInteraction " + name);
    }

    public void onMessageContextInteractionEvent(@NotNull MessageContextInteractionEvent event) {

    }


    public String getName() {
        return name;
    }
}
