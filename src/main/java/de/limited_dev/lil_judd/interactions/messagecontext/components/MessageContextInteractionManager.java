package de.limited_dev.lil_judd.interactions.messagecontext.components;

import de.limited_dev.lil_judd.interactions.messagecontext.GetMessageIDInteraction;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MessageContextInteractionManager {

    private static List<MessageContext> interactions = new CopyOnWriteArrayList<>();

    public static void registerInteractions() {
        interactions.add(new GetMessageIDInteraction());
    }

    public static List<MessageContext> getInteractions() {
        return interactions;
    }
}
