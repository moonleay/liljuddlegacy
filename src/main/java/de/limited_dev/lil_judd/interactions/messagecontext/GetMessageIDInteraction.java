package de.limited_dev.lil_judd.interactions.messagecontext;

import de.limited_dev.lil_judd.interactions.messagecontext.components.MessageContext;
import net.dv8tion.jda.api.events.interaction.command.MessageContextInteractionEvent;
import org.jetbrains.annotations.NotNull;

public class GetMessageIDInteraction extends MessageContext {
    public GetMessageIDInteraction() {
        super("Get message ID");
    }

    @Override
    public void onMessageContextInteractionEvent(@NotNull MessageContextInteractionEvent event) {
        event.reply("Message ID: " + event.getTarget().getIdLong()).queue();
    }
}
