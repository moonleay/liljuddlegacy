package de.limited_dev.lil_judd.interactions.usercontext;


import de.limited_dev.lil_judd.interactions.usercontext.components.UserContext;
import net.dv8tion.jda.api.events.interaction.command.UserContextInteractionEvent;
import org.jetbrains.annotations.NotNull;

public class GetPFPInteraction extends UserContext {
    public GetPFPInteraction() {
        super("Get user avatar");
    }

    @Override
    public void onUserContextInteraction(@NotNull UserContextInteractionEvent event) {
        event.reply("Avatar: " + event.getTarget().getEffectiveAvatarUrl()).queue();
    }
}
