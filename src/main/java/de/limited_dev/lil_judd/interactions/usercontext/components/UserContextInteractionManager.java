package de.limited_dev.lil_judd.interactions.usercontext.components;

import de.limited_dev.lil_judd.interactions.usercontext.GetPFPInteraction;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class UserContextInteractionManager {
    private static List<UserContext> interactions = new CopyOnWriteArrayList<>();

    public static void registerInteractions() {
        interactions.add(new GetPFPInteraction());
    }

    public static List<UserContext> getInteractions() {
        return interactions;
    }
}
