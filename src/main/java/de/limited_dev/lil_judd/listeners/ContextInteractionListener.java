package de.limited_dev.lil_judd.listeners;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.interactions.messagecontext.components.MessageContext;
import de.limited_dev.lil_judd.interactions.messagecontext.components.MessageContextInteractionManager;
import de.limited_dev.lil_judd.interactions.usercontext.components.UserContext;
import de.limited_dev.lil_judd.interactions.usercontext.components.UserContextInteractionManager;
import de.limited_dev.lil_judd.util.Logger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.MessageContextInteractionEvent;
import net.dv8tion.jda.api.events.interaction.command.UserContextInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class ContextInteractionListener extends ListenerAdapter {

    private static final Logger lgr = Main.getLgr();

    @Override
    public void onUserContextInteraction(@NotNull UserContextInteractionEvent event) {
        Member target = event.getInteraction().getTargetMember();
        Member source = event.getMember();
        Guild g = event.getGuild();

        lgr.info("New User Interaction from " + source.getUser().getName() + " on " + target.getUser().getName() + " in " + g.getName());
        for (UserContext uc : UserContextInteractionManager.getInteractions())
            if (event.getName().equals(uc.getName()))
                uc.onUserContextInteraction(event);

    }

    @Override
    public void onMessageContextInteraction(@NotNull MessageContextInteractionEvent event) {
        Member source = event.getMember();
        Guild g = event.getGuild();

        lgr.info("New Message Interaction from" + source.getUser().getName() + " on " + event.getInteraction().getMember().getUser().getName() + " in " + g.getName());
        for (MessageContext mc : MessageContextInteractionManager.getInteractions())
            if (event.getName().equals(mc.getName()))
                mc.onMessageContextInteractionEvent(event);
    }
}
