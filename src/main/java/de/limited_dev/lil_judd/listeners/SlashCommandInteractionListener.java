package de.limited_dev.lil_judd.listeners;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommandManager;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class SlashCommandInteractionListener extends ListenerAdapter {

    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        Main.getLgr().info("New Command: /" + event.getName() + " in " + event.getGuild().getName());
        for(JDACommand c : JDACommandManager.getCommands())
            if(event.getName().equals(c.getName()))
                c.onSlashCommand(event);
    }
}
