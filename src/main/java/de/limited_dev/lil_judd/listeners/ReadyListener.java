package de.limited_dev.lil_judd.listeners;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.features.planner.TimePlanner;
import de.limited_dev.lil_judd.features.storage.GuildTimePlannerStorage;
import de.limited_dev.lil_judd.util.Logger;
import de.limited_dev.lil_judd.util.SlashCommandRegister;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class  ReadyListener extends ListenerAdapter {
    private final Logger lgr = Main.getLgr();
    private final GuildTimePlannerStorage gtps = TimePlanner.getGtps();

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        String usernameOfSelf = Main.getJda().getSelfUser().getName();

        SlashCommandRegister.registerCommands();

        lgr.info("Logged into: " + usernameOfSelf);
        lgr.info(usernameOfSelf + " is ready for Freddy");
    }
}
