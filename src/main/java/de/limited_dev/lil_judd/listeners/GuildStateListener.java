package de.limited_dev.lil_judd.listeners;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.util.Logger;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class GuildStateListener extends ListenerAdapter {
    private Logger lgr = Main.getLgr();

    @Override
    public void onGuildJoin(@NotNull GuildJoinEvent event) {
        lgr.info("I have been added to a guild.");
        if(event.getGuild().getName().contains("Cast") || event.getGuild().getName().contains("cst")){
            event.getGuild().leave().queue();
        }
    }

    @Override
    public void onGuildLeave(@NotNull GuildLeaveEvent event) {
        lgr.info("I have been removed from a guild.");
    }
}
