package de.limited_dev.lil_judd.commands.consolecommand.util;

import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;
import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommandManager;

import java.util.List;

public class HelpConsoleCommand extends ConsoleCommand {
    public HelpConsoleCommand() {
        super("help", "Shows all commands", null);
    }

    @Override
    public void onCommand(String[] args) {
        String out = "List of available console commands:\n";
        List<ConsoleCommand> commandList = ConsoleCommandManager.getCommands();
        for(int i = 0; i < commandList.size(); ++i){
            out = out + commandList.get(i).getName() + (i == commandList.size() - 1 ? "" : ",");
        }
        lgr.consoleOut(out);
    }
}
