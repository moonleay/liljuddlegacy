package de.limited_dev.lil_judd.commands.consolecommand.system;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;
import de.limited_dev.lil_judd.features.planner.TimePlanner;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.managers.AudioManager;

public class ShutdownCommand extends ConsoleCommand {
    public ShutdownCommand() {
        super("shutdown", "Shut the bot down", null);
    }

    @Override
    public void onCommand(String[] args) {
        Main.shutdown();
    }
}
