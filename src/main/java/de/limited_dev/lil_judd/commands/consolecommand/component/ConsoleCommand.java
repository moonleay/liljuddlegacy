package de.limited_dev.lil_judd.commands.consolecommand.component;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.util.Logger;
import net.dv8tion.jda.api.JDA;

public class ConsoleCommand {
    private final String name;
    private final String description;
    private final String[] args;
    protected final Logger lgr = Main.getLgr();
    protected final JDA jda = Main.getJda();

    public ConsoleCommand(String name, String description, String[] args){
        this.name = name;
        this.description = description;
        this.args = args;
        lgr.info("Created console command " + name);
    }

    public void onCommand(String[] args){

    }

    protected void sendUsage(String commandName){
        ConsoleCommand command = ConsoleCommandManager.getCommand(commandName);
        if(command == null){
            sendUsage(this.getName());
            return;
        }
        String out = "Command: " + command.getName() +  " = " + command.getDescription() + "";
        lgr.consoleOut(out);

        out = "Usage: " + commandName;
        if(command.getArgs() == null){
            lgr.consoleOut(out);
            return;
        }
        out = out + (command.getArgs().length == 0 ? "" : " ");
        for(int i = 0; i < command.getArgs().length; ++i){
            out = out + "<" + command.getArgs()[i] + ">" + (command.getArgs().length - 1 == i ? "" : " ");
        }
        lgr.consoleOut(out);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String[] getArgs() {
        return args;
    }
}
