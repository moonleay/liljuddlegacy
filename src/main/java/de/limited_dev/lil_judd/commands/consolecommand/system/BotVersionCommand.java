package de.limited_dev.lil_judd.commands.consolecommand.system;

import de.limited_dev.lil_judd.build.BuildConstants;
import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;

public class BotVersionCommand extends ConsoleCommand {
    public BotVersionCommand() {
        super("version", "Shows the bot version", null);
    }

    @Override
    public void onCommand(String[] args) {
        lgr.consoleOut(jda.getSelfUser().getName() + " on " + BuildConstants.botVersion);
    }
}
