package de.limited_dev.lil_judd.commands.consolecommand.util;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;

public class AnnounceCommand extends ConsoleCommand {
    public AnnounceCommand() {
        super("announce", "Announce something to the default channel", new String[]{"Message"});
    }

    @Override
    public void onCommand(String[] args) {
        String ar = "";
        for(int i = 0; i < args.length; ++i){
            ar = ar + " " +  args[i];
        }

        for(Guild g : Main.getJda().getSelfUser().getMutualGuilds()){
            EmbeddedMessageHelper.sendSimpleOneLiner(g, g.getDefaultChannel().asTextChannel().getIdLong(),
                    "Announcement", ar, null);
        }
    }
}
