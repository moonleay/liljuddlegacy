package de.limited_dev.lil_judd.commands.consolecommand.util;

import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;

public class ManualCommand extends ConsoleCommand {
    public ManualCommand() {
        super("man", "Show the correct usage of a command & its description", new String[]{"Command Name"});
    }

    @Override
    public void onCommand(String[] args) {
        if(args.length != 1){
            this.sendUsage(this.getName());
            return;
        }
        String commandName = args[0];
        this.sendUsage(commandName.toLowerCase());
    }
}
