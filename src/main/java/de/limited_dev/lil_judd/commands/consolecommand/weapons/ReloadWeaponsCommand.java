package de.limited_dev.lil_judd.commands.consolecommand.weapons;

import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;
import de.limited_dev.lil_judd.features.weapons.WeaponManager;

public class ReloadWeaponsCommand extends ConsoleCommand {
    public ReloadWeaponsCommand() {
        super("reloadweapons", "Reload Weapons found in files", null);
    }

    @Override
    public void onCommand(String[] args) {
        WeaponManager.reloadWeapons();
    }
}
