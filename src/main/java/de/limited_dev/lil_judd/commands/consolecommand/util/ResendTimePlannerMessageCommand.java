package de.limited_dev.lil_judd.commands.consolecommand.util;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.commands.consolecommand.component.ConsoleCommand;
import de.limited_dev.lil_judd.features.planner.TimePlanner;
import de.limited_dev.lil_judd.features.storage.GuildTimePlannerStorage;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.emoji.Emoji;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ResendTimePlannerMessageCommand extends ConsoleCommand {
    private static final GuildTimePlannerStorage gtps = Main.getGtps();

    public ResendTimePlannerMessageCommand() {
        super("resendplannermsg", "Resend Time Planner Message", new String[]{"guildID / all"});
    }

    @Override
    public void onCommand(String[] args) {
        if (args.length != 1) {
            this.sendUsage(this.getName());
            return;
        }
        lgr.consoleOut("Resending the msg to " + args[0]);
        if (args[0].toLowerCase().equals("all")) {
            for (Guild g : jda.getSelfUser().getMutualGuilds()) {
                sendMessageToGuild(g);
                lgr.consoleOut("Sent message to " + g.getName());
            }
            lgr.consoleOut("Done.");
            return;
        }
        Guild g = jda.getGuildById(Long.parseLong(args[0]));
        if (g == null) {
            lgr.consoleOut("This guild does not exist.");
            return;
        }
        sendMessageToGuild(g);
        lgr.consoleOut("The message has been send to " + g.getName());
        lgr.consoleOut("Done.");
    }

    private void sendMessageToGuild(Guild g) {
        if (g == null) {
            lgr.info("Guild does not exist");
            return;
        }
        long l = g.getIdLong();
        if (g.getTextChannelById(gtps.guildsWithPlanner.get(l)) == null) {
            lgr.info("The channel with the id " + gtps.guildsWithPlanner.get(l) + " does not exist.");
            return;
        }
        ZonedDateTime then = ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(TimePlanner.hour).withMinute(TimePlanner.minute).withSecond(TimePlanner.second);
        EmbeddedMessageHelper.sendSimpleOneLiner(g, gtps.guildsWithPlanner.get(l), "Timeplanning System", "Do you have time on the following Days?", null);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        for (int i = 0; i < 7; ++i) {
            if (i != 0)
                then = then.plusDays(1).withHour(TimePlanner.hour).withMinute(TimePlanner.minute).withSecond(TimePlanner.second);
            MessageEmbed eb = EmbeddedMessageHelper.getEmbeddedMessageAutomated(false, null, then.getDayOfWeek() + ", " + then.getDayOfMonth() + "." + then.getMonthValue() + "." + then.getYear(), null, false).build();
            g.getTextChannelById(gtps.guildsWithPlanner.get(l)).sendMessageEmbeds(eb).queue(this::addReactions);
            lgr.info("[" + l + ", " + gtps.guildsWithPlanner.get(l) + "]" + ": " + then.getDayOfWeek() + ", " + then.getDayOfMonth() + "." + then.getMonthValue() + "." + then.getYear());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        lgr.info("Sent to Server " + g.getName());
    }

    private void addReactions(Message msg) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        msg.addReaction(Emoji.fromUnicode("✅")).queue();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        msg.addReaction(Emoji.fromFormatted("❌")).queue();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        msg.addReaction(Emoji.fromFormatted("❓")).queue();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
