package de.limited_dev.lil_judd.commands.jdacommands.management;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.planner.TimePlanner;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class RemoveFeatureCommand extends JDACommand {
    public RemoveFeatureCommand() {
        super("remove", "Remove a feature");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        if(!event.getGuild().getMemberById(event.getUser().getId()).hasPermission(Permission.ADMINISTRATOR)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "403: Forbidden", "Sorry, but you don't have the Permission to run this command", null);
            return;
        }
        if(event.getOption("feature").getAsString().equals("timefinder")){
            TimePlanner.getGtps().guildsWithPlanner.remove(event.getGuild().getIdLong());
            TimePlanner.getGtps().save();
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "The Feature has been disabled",
                    "The Feature has been removed from this Server", null);
        }
    }
}
