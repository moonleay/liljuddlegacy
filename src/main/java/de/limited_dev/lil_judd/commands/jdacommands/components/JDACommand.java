package de.limited_dev.lil_judd.commands.jdacommands.components;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.util.Logger;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class JDACommand {

    private final String name;
    private final String description;
    protected final Logger lgr = Main.getLgr();


    public JDACommand(String name, String description) {
        this.name = name;
        this.description = description;
        lgr.info("Created jda command " + name);
    }

    public void onSlashCommand(SlashCommandInteractionEvent event) {

    }

    public String getName() {
        return name.toLowerCase();
    }

    public String getDescription() {
        return description;
    }
}
