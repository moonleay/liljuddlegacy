package de.limited_dev.lil_judd.commands.jdacommands.util;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommandManager;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class HelpCommand extends JDACommand {
    public HelpCommand() {
        super("help", "Shows all commands & their descriptions");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        HashMap<String, String[]> values = new HashMap<>();
        CopyOnWriteArrayList<String> arguments = new CopyOnWriteArrayList<>();
        CopyOnWriteArrayList<String> disc = new CopyOnWriteArrayList<>();
        for (JDACommand c : JDACommandManager.getCommands()) {
            String commandArgs = c.getName();
            arguments.add(commandArgs);

            disc.add(c.getDescription().isEmpty() ? "No description" : c.getDescription());
        }
        String[] argumentsArray = new String[arguments.size()];
        for (int i = 0; i < arguments.size(); i++) {
            argumentsArray[i] = arguments.get(i);
        }
        values.put("Command", argumentsArray);

        String[] Descriptions = new String[disc.size()];
        for (int i = 0; i < disc.size(); i++) {
            Descriptions[i] = disc.get(i);
        }
        values.put("Description", Descriptions);

        EmbeddedMessageHelper.sendWithTable(event, "Uses / commands", "Commands", Color.MAGENTA,
                null, "List of avalible commands", new String[]{"Command", "Description"}, values, true);
    }
}
