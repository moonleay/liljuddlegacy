package de.limited_dev.lil_judd.commands.jdacommands.weapons;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.weapons.WeaponManager;
import de.limited_dev.lil_judd.features.weapons.components.WeaponKit;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;

public class WeaponInfoCommand extends JDACommand {
    public WeaponInfoCommand() {
        super("weaponinfo", "Get infos about a weapon");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        WeaponKit wk = WeaponManager.getWeaponKitFromSearch(event.getOption("name").getAsString());
        if(wk == null){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "404: Not found", "This weapon does not exist.", null);
            return;
        }
        HashMap<String, String[]> hm = new HashMap<>();
        hm.put("Information", new String[]{"Callout", "Required Level", "Points for special", "Main Type", "Sub Type", "Special Type"});
        hm.put("Value", new String[]{wk.getCallout(), wk.getReqiredLevel() + "", wk.getPointsForSpecial() + "p", wk.getMainType().getType(), wk.getSubType().getNameEN(), wk.getSpecialType().getNameEN()});

        EmbeddedMessageHelper.sendWithTable(event, Main.getJda().getSelfUser().getName(),wk.getNameEN(), Color.GREEN, wk.getImageLink(),"Based on Splatoon patch ver." + Main.getWeaponDataManager().getBasedOnSplatoonVersion(),new String[]{"Information", "Value"}, hm,true);
    }
}
