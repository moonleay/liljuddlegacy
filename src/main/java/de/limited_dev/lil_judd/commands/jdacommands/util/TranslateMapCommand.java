package de.limited_dev.lil_judd.commands.jdacommands.util;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.maps.MapManager;
import de.limited_dev.lil_judd.features.maps.components.MapData;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class TranslateMapCommand extends JDACommand {
    public TranslateMapCommand() {
        super("mapinfo", "Show map information");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        String map = event.getOption("map").getAsString().toLowerCase();
        MapData md = MapManager.getMapData(map);
        if (md == null) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "404: Not found", "This Map does not exist.\nCheck for spelling mistakes", null);
            return;
        }
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Map Translator", "ENG: " + md.getNameEN() + "" +
                "\nDE: " + md.getNameDE(), md.getMapLink());
    }
}
