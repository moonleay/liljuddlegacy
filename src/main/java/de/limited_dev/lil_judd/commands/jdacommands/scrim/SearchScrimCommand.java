package de.limited_dev.lil_judd.commands.jdacommands.scrim;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.scrims.ScrimMaker;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SearchScrimCommand extends JDACommand {
    public SearchScrimCommand() {
        super("searchscrim", "");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        User u1 = event.getOption("player1").getAsUser();
        User u2 = event.getOption("player2").getAsUser();
        User u3 = event.getOption("player3").getAsUser();
        User u4 = event.getOption("player4").getAsUser();
        List<User> users = new CopyOnWriteArrayList<>(){{
            add(u1);
            add(u2);
            add(u3);
            add(u4);
        }};
        Guild g = event.getGuild();
        int div = event.getOption("div").getAsInt();
        if(ScrimMaker.IsInGameOrQueue(g)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "406: Not Acceptable", "The Guild is already in a match.", null);
            return;
        }
        if(ContainsAUserTwice(users)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "406: Not Acceptable", "Please only add every user once.", null);
            return;
        }
        if(ContainsABot(users)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "406: Not Acceptable", "You cannot add a bot as user.", null);
            return;
        }
        ScrimMaker.EnterQueue(g, event.getChannel().asTextChannel(), div, users);
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "<a:loading:1028062794018476093> Queuing... ",
                g.getName() + " has been added to the Scrim Queue for Division " + div + "\n\n" +
                        "Player 1 >> " + u1.getAsMention() + "\n" +
                        "Player 2 >> " + u2.getAsMention() + "\n" +
                        "Player 3 >> " + u3.getAsMention() + "\n" +
                        "Player 4 >> " + u4.getAsMention(),
                null);
    }


    private boolean ContainsAUserTwice(List<User> users){
        int counter = 0;
        for (User u : users)
            for (User us : users)
                if(u.getIdLong() == us.getIdLong())
                    ++counter;
        return counter != 4;
    }

    private boolean ContainsABot(List<User> users){
        for (User u : users)
            if(u.isBot() || u.isSystem())
                return true;
        return false;
    }
}
