package de.limited_dev.lil_judd.commands.jdacommands.scrim;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.scrims.ScrimMaker;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class EndScrimCommand extends JDACommand {
    public EndScrimCommand() {
        super("endscrim", "");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        User u = event.getUser();
        if(!ScrimMaker.IsInGameOrQueue(g)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "405: Method not allowed", "The Guild wasn't in a scrim.", null);
            return;
        }
        if(!ScrimMaker.IsUserInGameOrQueue(g, u)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "403: Forbidden", "There is a game running, but you are not part of it.", null);
            return;
        }
        ScrimMaker.LeaveQueue(g);
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "The Scrim has ended.", "You ended the Scrim.", null);
    }
}
