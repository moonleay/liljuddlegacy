package de.limited_dev.lil_judd.commands.jdacommands.scrim;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.scrims.ScrimMaker;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class MessageScrimCommand extends JDACommand {
    public MessageScrimCommand() {
        super("msgscrim", "");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        Guild g = event.getGuild();
        User u = event.getUser();
        if(!ScrimMaker.IsInGameOrQueue(g)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "404: Not Found", "The Guild is not in a scrim.", null);
            return;
        }
        if(!ScrimMaker.IsUserInGame(g, u)){
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "403: Forbidden", "You are not in the current game.", null);
            return;
        }
        ScrimMaker.sendMessageToEnemy(g, u, event.getOption("message").getAsString());
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Message to " + ScrimMaker.getEnemyGuild(g).getName(), u.getName() + "#" + u.getDiscriminator() + " >> " + event.getOption("message").getAsString(), u.getAvatarUrl());
        //EmbeddedMessageHelper.sendSimpleOneLiner(this.enemyGuildInQueue.getGuild(), this.enemyGuildInQueue.getResponseChannel().getIdLong(), "Message from " + this.enemyGuildInQueue.getGuild().getName(), u.getName() + "#" + u.getDiscriminator() + " >> " + msg, u.getAvatarUrl());
    }
}
