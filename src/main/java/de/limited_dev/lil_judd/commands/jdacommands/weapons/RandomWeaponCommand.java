package de.limited_dev.lil_judd.commands.jdacommands.weapons;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.weapons.WeaponManager;
import de.limited_dev.lil_judd.features.weapons.components.WeaponKit;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class RandomWeaponCommand extends JDACommand {
    public RandomWeaponCommand() {
        super("randomweapon", "Get a random weapon");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        WeaponKit wk = WeaponManager.getIndex((int)(Math.random()*((WeaponManager.getSize() - 1)-0+1)+0));
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "Random Weapon", "Your random Weapon is\n**" + wk.getNameEN() + "**", wk.getImageLink());
    }
}
