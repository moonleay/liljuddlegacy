package de.limited_dev.lil_judd.commands.jdacommands.components;

import de.limited_dev.lil_judd.commands.jdacommands.management.RemoveFeatureCommand;
import de.limited_dev.lil_judd.commands.jdacommands.management.SetupFeatureCommand;
import de.limited_dev.lil_judd.commands.jdacommands.util.HelpCommand;
import de.limited_dev.lil_judd.commands.jdacommands.util.InfoCommand;
import de.limited_dev.lil_judd.commands.jdacommands.util.TranslateMapCommand;
import de.limited_dev.lil_judd.commands.jdacommands.weapons.RandomWeaponCommand;
import de.limited_dev.lil_judd.commands.jdacommands.weapons.WeaponInfoCommand;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class JDACommandManager {

    private static List<JDACommand> commands = new CopyOnWriteArrayList<>();

    public static void registerCommands(){
        //commands.add(new SearchScrimCommand());
        //commands.add(new EndScrimCommand());
        //commands.add(new MessageScrimCommand());
        //commands.add(new ScrimInfoCommand());
        commands.add(new InfoCommand());
        commands.add(new SetupFeatureCommand());
        commands.add(new RemoveFeatureCommand());
        commands.add(new TranslateMapCommand());
        commands.add(new WeaponInfoCommand());
        commands.add(new RandomWeaponCommand());
        commands.add(new HelpCommand());
    }

    public static List<JDACommand> getCommands() {
        return commands;
    }
}
