package de.limited_dev.lil_judd.commands.jdacommands.scrim;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.scrims.ScrimMaker;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class ScrimInfoCommand extends JDACommand {
    public ScrimInfoCommand() {
        super("scriminfo", "");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        EmbeddedMessageHelper.sendSimpleOneLiner(event, "ScrimMaker info",
                "Guilds in queue: " + ScrimMaker.getQueueSize() + "\n" +
                "Guild in game: " + ScrimMaker.getGameSize() + "\n" +
                        "Currently Active Users in Scrim: " + (ScrimMaker.getQueueSize() * 4 + ScrimMaker.getGameSize() * 4),null);
    }
}
