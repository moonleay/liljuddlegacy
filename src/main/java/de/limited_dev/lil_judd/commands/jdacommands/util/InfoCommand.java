package de.limited_dev.lil_judd.commands.jdacommands.util;

import de.limited_dev.lil_judd.Main;
import de.limited_dev.lil_judd.build.BuildConstants;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommandManager;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import de.limited_dev.lil_judd.util.TimeUtil;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.awt.*;
import java.util.HashMap;

public class InfoCommand extends JDACommand {
    public InfoCommand() {
        super("info", "Shows Info about the bot");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        HashMap<String, String[]> hm = new HashMap<>();
        hm.put("Information", new String[]{"Version", "JDA version", "Ping", "Uptime", "RAM", "# Commands"});
        hm.put("Value", new String[]{BuildConstants.botVersion, BuildConstants.jdaVersion, Main.getJda().getGatewayPing() + "ms", TimeUtil.getTimeFormatedShortend(Main.getUptime()),
                (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 / 1024 + "MB / " + (Runtime.getRuntime().maxMemory() - (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())) / 1024 / 1024 + "MB; " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) * 100L / Runtime.getRuntime().maxMemory() + "%"
                , JDACommandManager.getCommands().size() + ""});
        EmbeddedMessageHelper.sendWithTable(event, Main.getJda().getSelfUser().getName() + " " + BuildConstants.botVersion, "Bot information", Color.YELLOW, Main.getJda().getSelfUser().getAvatarUrl(), "General information about the bot",
                new String[]{"Information", "Value"}, hm, true);
    }
}
