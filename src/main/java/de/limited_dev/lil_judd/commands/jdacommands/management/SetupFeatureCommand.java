package de.limited_dev.lil_judd.commands.jdacommands.management;

import de.limited_dev.lil_judd.commands.jdacommands.components.JDACommand;
import de.limited_dev.lil_judd.features.planner.TimePlanner;
import de.limited_dev.lil_judd.util.EmbeddedMessageHelper;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class SetupFeatureCommand extends JDACommand {
    public SetupFeatureCommand() {
        super("setup", "Setup a feature");
    }

    @Override
    public void onSlashCommand(SlashCommandInteractionEvent event) {
        if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "403: Forbidden", "You are not allowed to run this command.", null);
            return;
        }
        if (event.getOption("feature").getAsString().equals("timefinder")) {
            TimePlanner.getGtps().guildsWithPlanner.put(event.getGuild().getIdLong(), event.getOption("channel").getAsChannel().getIdLong());
            TimePlanner.getGtps().save();
            EmbeddedMessageHelper.sendSimpleOneLiner(event, "Feature added to channel.",
                    "\"" + event.getOption("channel").getAsChannel().getName() + "\" has been added to send time notifications.", null);
        }
    }
}
