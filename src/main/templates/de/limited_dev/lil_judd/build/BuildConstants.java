package de.limited_dev.lil_judd.build;

public class BuildConstants{
    public static String botVersion = "${version}";
    public static String splatoonPatchVersion = "${splatoonPatchBased}";
    public static String jdaVersion = "${jdaVersion}";
}