import org.jetbrains.gradle.ext.ProjectSettings
import org.jetbrains.gradle.ext.TaskTriggersConfig

plugins {
    java
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.1.6"
    `maven-publish`
}

group = "de.limited_dev"
version = System.getenv("CI_COMMIT_TAG") ?: System.getenv("CI_COMMIT_SHORT_SHA")?.let { "$it-dev" }
        ?: "DevelopmentBuild" //"v1.5.5-build1"
var splatoonPatchBased = "2.0.0"
var jdaVersion = "5.0.0-alpha.20"
val mavenArtifact = "lil_Judd"
project.base.archivesName.set(mavenArtifact)

repositories {
    mavenCentral()
    maven("https://m2.dv8tion.net/releases")
    maven {
        name = "Gitlab"
        val projectId = System.getenv("CI_PROJECT_ID")
        val apiV4 = System.getenv("CI_API_V4_URL")
        url = uri("https://$apiV4/projects/$projectId/packages/maven")
        authentication {
            create("header", HttpHeaderAuthentication::class.java) {
                if (System.getenv("CI_JOB_TOKEN") != null) {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                } else {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Private-Token"
                        value = project.ext["myGitlabToken"] as String
                    }
                }
            }
        }
    }
}

dependencies {
    implementation("net.dv8tion:JDA:$jdaVersion")
    implementation("org.slf4j:slf4j-api:2.0.3")
    implementation("org.slf4j:slf4j-simple:2.0.3")
}

val templateSource = file("src/main/templates")
val templateDest = layout.buildDirectory.dir("generated/sources/templates")

tasks {
    getByName<Test>("test") {
        useJUnitPlatform()
    }

    withType<Jar> {
        manifest {
            attributes["Main-Class"] = "de.limited_dev.lil_judd.Main"
        }
        // To add all of the dependencies
        from(sourceSets.main.get().output)

        dependsOn(configurations.runtimeClasspath)
        from({
            configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
        })
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
        dependsOn("generateTemplates", "processResources")
    }

    withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
        dependencies {
            include(dependency("net.dv8tion:JDA:$jdaVersion"))
            include(dependency("org.slf4j:slf4j-api:2.0.3"))
            include(dependency("org.slf4j:slf4j-simple:2.0.3"))
        }
        dependsOn("generateTemplates", "processResources")
    }

    withType<JavaCompile>{
        options.encoding = "UTF-8"
        dependsOn("generateTemplates")
    }

    create<Copy>("generateTemplates") {
        filteringCharset = "UTF-8"
        val props = mapOf(
            "version" to version,
            "splatoonPatchBased" to splatoonPatchBased,
            "jdaVersion" to jdaVersion
        )
        inputs.properties(props)
        from(templateSource)
        expand(props)
        into(templateDest)
    }

    withType<Jar> {
        from("LICENSE") {
            rename { "${it}_${project.base.archivesName.get()}" }
        }

        archiveBaseName.set(mavenArtifact)

        dependsOn("generateTemplates", "processResources")
    }
}

sourceSets.main {
    java {
        srcDir(templateDest)
    }

    resources {
        srcDir("src/main/generated")
    }
}

java{
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            version = project.version as String
            artifactId = mavenArtifact
            from(components["java"])
        }
    }

    repositories {
        if (System.getenv("CI_JOB_TOKEN") != null) {
            maven {
                name = "GitLab"
                val projectId = System.getenv("CI_PROJECT_ID")
                val apiV4 = System.getenv("CI_API_V4_URL")
                url = uri("$apiV4/projects/$projectId/packages/maven")
                authentication {
                    create("token", HttpHeaderAuthentication::class.java) {
                        credentials(HttpHeaderCredentials::class.java) {
                            name = "Job-Token"
                            value = System.getenv("CI_JOB_TOKEN")
                        }
                    }
                }
            }
        }
    }
}

rootProject.idea.project {
    this as ExtensionAware
    configure<org.jetbrains.gradle.ext.ProjectSettings> {
        this as ExtensionAware
        configure<org.jetbrains.gradle.ext.TaskTriggersConfig> {
            afterSync(tasks["generateTemplates"], tasks["processResources"])
        }
    }
}
//fuck eclipse users amirite?
//rootProject.eclipse.synchronizationTasks("generateTemplates")